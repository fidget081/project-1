import { saveToStorage } from './login';

const loginBlock = document.querySelector('#loginBlock');

const pageSignUp = document.querySelector('#step1Block');

const regBlock = document.querySelector('#regBlock');

const regBtn = document.querySelector('#regBtn');

const stepBack = document.querySelector('#toLoginSvg');

const btnContinue = document.querySelector('#toStep2Btn');

const rebtnContinue = document.querySelector('#from3to2Svg');


function BlockActive() {
  loginBlock.setAttribute('style', 'display: none;');
  pageSignUp.setAttribute('style', 'display: flex;');
};

function reBlockActive(){
  pageSignUp.setAttribute('style', 'display: none;');
  loginBlock.setAttribute('style', 'display: flex;');
}

function regBlockContinueActive(){
  pageSignUp.setAttribute('style', 'display: none;');
  regBlock.setAttribute('style', 'display: flex;');
}

function reBlockActive2(){
  regBlock.setAttribute('style', 'display: none;');
  pageSignUp.setAttribute('style', 'display: flex;');
}


regBtn.addEventListener("click", () => BlockActive());

stepBack.addEventListener("click", () => reBlockActive());

btnContinue.addEventListener("click", () => regBlockContinueActive());

rebtnContinue.addEventListener("click", () => reBlockActive2());

// console.log('wizard');


// task-2

const ls = window.localStorage;

const typeForm = document.querySelector('.form-user-type');

const typeStudent = typeForm.querySelector('#user_student');
//const typeTeacher = typeForm.querySelector('#user_teacher');

const objType = {};

btnContinue.addEventListener('click', function(event){
  event.preventDefault();

  if (typeStudent.checked) {
    ls.setItem('type', 'Student');
  } else {
    ls.setItem('type', 'Teacher');
  }

  objType.type = ls.getItem('type');
  
  return objType;
});


const loginForm = document.querySelector('.form-login');
const inputName = document.querySelector('#name');
const inputEmail = document.querySelector('#email');
const inputPass = document.querySelector('#password');
const inputPassNext = document.querySelector('#password_next');
const btnCreateAcc = document.querySelector('#createAccount');

btnCreateAcc.addEventListener('click', function(event){
  event.preventDefault();

  const nameValue = inputName.value;
  const emailValue = inputEmail.value.trim();
  const passValue = inputPass.value.trim();
  const passValueNext = inputPassNext.value.trim();

  if(!nameValue || !emailValue || !passValue || !passValueNext){
    throw new Error ('No data!');
  }

  ls.setItem('name', JSON.stringify(nameValue));
  const nameV = JSON.parse(ls.getItem('name'));

  const arrNameValue = nameV.split(" ");

  if(arrNameValue[0].length < 2 || arrNameValue[1].length < 2){
    throw new Error ('No data!');
  }

  // if(typeof arrNameValue[0] !== 'string' || typeof arrNameValue[1] !== 'string'){
  //   throw new Error ('No string');
  // }

  const [firstName, lastName] = arrNameValue;
      
  objType.name = (`${firstName} ${lastName}`);

  ls.setItem('email', JSON.stringify(emailValue));
  objType.email = JSON.parse(ls.getItem('email'));
  

  ls.setItem('password', JSON.stringify(passValue));
  const pass = JSON.parse(ls.getItem('password'));

  ls.setItem('passwordnext', JSON.stringify(passValueNext));
  const passn = JSON.parse(ls.getItem('passwordnext'));
  
  if (pass !== passn){
    throw new Error('password Error');
  }

  objType.password = pass;
  console.log(objType);

  ls.clear();
});

